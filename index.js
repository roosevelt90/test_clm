const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const moviesRoutes = require('./routes/movies.routes');
const logger = require('koa-logger');
const { ui } = require("swagger2-koa");
const swagger = require("swagger2");
const swaggerDocument = swagger.loadDocumentSync("api.yaml");


const port = 3000;
const app = new Koa();  
app.use(ui(swaggerDocument, "/swagger"))
app.use(bodyParser());
app.use(logger())
app.use(moviesRoutes.routes()).use(moviesRoutes.allowedMethods());
app.listen(port);
console.log("Application is running on port " + port);