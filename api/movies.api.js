const { getByTitle, getAllMovies, getMovieAndUpdate } = require('../dao/movies.dao');


const getMovie = async (title, year) => {

    return await getByTitle(title, year);
}

const getMovies = async (page) => {

    return await getAllMovies(page);
}

const update = async (find, replace) => {

    return await getMovieAndUpdate(find, replace);
}

module.exports = {
    getMovie,
    getMovies,
    update
}