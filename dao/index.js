const {MongoClient} = require("mongodb");

var mongoDB = 'mongodb://127.0.0.1:27017';

const client = new MongoClient(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

client.connect(err => {
    if(err){
        console.error(err);
        process.exit(-1);
    }
    console.log("Successfully connected to MongoDB");
})

module.exports = client;