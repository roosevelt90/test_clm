const movie = require('./index').db('clm_db').collection('movies');

const getByTitle = async (title, year) => {
    var arrWhere = {
        title: { "$regex": title, "$options": "i" }  
    };
    if (year) {
        arrWhere.yearReleased = year;
    }
    var rtn = await movie.findOne(arrWhere);
    return rtn;
}

const getAllMovies = async (page) => {
    var skip = 0;
    if (page > 1) {
        skip = ((page - 1) * 5);
    }
    var rtn = await movie.find().limit(5).skip(skip).toArray();
    return rtn;
}

const getMovieAndUpdate = async (find, replace) => {
    var rtn = await movie.updateOne(
        { plot: { "$regex": find, "$options": "i" } },
        {
            $set: { plot: replace },
            $currentDate: { lastModified: true }
        }
    );
    return rtn;
}


module.exports = { getByTitle, getAllMovies, getMovieAndUpdate };