const Router = require("@koa/router");
const Joi = require('joi');
const { getMovie, getMovies, update } = require('../api/movies.api');

const router = new Router({
    prefix: '/movies'
})

router.get('/find/:title', async ctx => {
    
    const title = ctx.params.title;
    const headers = ctx.request.header

    const schema = Joi.object({
        title: Joi.string().min(2).required(),
        year: Joi.string().min(4)
    });

    const dataToValidate = {
        title: title,
        year: headers.year
    };

    const validation = schema.validate(dataToValidate);
    if (validation.error) {
        ctx.status = 400;
        ctx.body = validation.error.details;
        return ctx;
    }

    ctx.body = await getMovie(title, headers.year);
})

router.get('/getall', async ctx => {
    const page = ctx.request.header.page
    const schema = Joi.object({
        page: Joi.number().required()
    });

    const dataToValidate = {
        page: page
    };

    const validation = schema.validate(dataToValidate);
    if (validation.error) {
        ctx.status = 400;
        ctx.body = validation.error.details;
        return ctx;
    }

    ctx.body = await getMovies(page);
})

router.post('/update', async ctx => {
    const find = ctx.request.body.find;
    const replace = ctx.request.body.replace;
    const schema = Joi.object({
        find: Joi.string().required(),
        replace: Joi.string().required()
    });

    const dataToValidate = {
        find: find,
        replace: replace
    };

    const validation = schema.validate(dataToValidate);
    if (validation.error) {
        ctx.status = 400;
        ctx.body = validation.error.details;
        return ctx;
    }

    ctx.body = await update(find, replace);
})

module.exports = router;